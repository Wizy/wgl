WGL - Fluid WebGL2 library
==========================

Introduction
------------
In WebGL (and OpenGL) transfering data by gl.texImage2d or gl.bufferData is asynchronous. Using a texture that is not yet ready will stall your application and introduce lags.  
WGL is a library that benefits from WebGL2 fence sync feature, to make WebGL a lag less experience.

Caveats
-------
Fences are nice, but doesn't give that much control over the opengl driver. Fence only assure that everything before their creation, has finished.
Meaning if you upload two textures, a big one and a small one right after, fences will consider the small one to be loaded only after the big one was.
In most cases, WGL will be slower than a conventional WebGL library. But WGL will assure the lowest render time, and highest / stable FPS.

Examples
--------
Thanks to ES6 await/async, WGL remains easy to use.  

The smallest example follows:  

```
<canvas id="canvas"></canvas>
<script src="https://cdnjs.cloudflare.com/ajax/libs/stats.js/r16/Stats.min.js"></script>
<script id="vertex" type="shader/vertex">
attribute vec3 aPos;

void main() {
    gl_Position = vec4(aPos, 1.0);
}
</script>
<script id="fragment" type="shader/vertex">
void main() {
    gl_FragColor = vec4(1.0, 0.75, 0.0, 1.0);
}
</script>
<script>

var context = new WGL.Context(document.querySelector('#canvas'));

var buffer, program;

// main loop
context.main(() => {
    context.clear();

    // draw triangle only when ready
    if(buffer && program) {
        program.use();
        buffer.draw();
    }
});

// async init for triangle
(async () => {
    program = await context.createProgram(document.querySelector('#vertex').innerText, document.querySelector('#fragment').innerText);
    buffer = await context.createBuffer([
        {
            data: [
                 0.0,  1.0,  0.0,
                -1.0, -1.0,  0.0,
                 1.0, -1.0,  0.0,
            ],
            attributes: {
                aPos: {size: 3},
            },
        },
    ]);
})();
</script>
```
