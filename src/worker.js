var i = 0;

onmessage = async function(event) {
    var task = event.data;
    if(task.command === 'createTexture') {
        var response = await fetch(task.url);
        var blob = await response.blob();
        var image = await createImageBitmap(blob, {
            imageOrientation: 'flipY', // none, flipY
            premultiplyAlpha: 'none', // none, premultiply, default
            colorspaceConversion: 'none', // none, default
        });

        function imageToUint8Array(image) {
            var canvas = new OffscreenCanvas(image.width, image.height),
                ctx = canvas.getContext('2d');
            canvas.width = image.width;
            canvas.height = image.height;

            ctx.drawImage(image, 0, 0);
            var imageData = ctx.getImageData(0, 0, image.width, image.height);
            var buff = new Uint8Array(imageData.data.buffer);

            return buff;
        }

        var data = imageToUint8Array(image);
        postMessage({
            width: image.width,
            height: image.height,
            buffer: data.buffer
        }, [data.buffer]);


        // TODO: generate mipmaps
    }
};
