(() => {
    var WGL = window.WGL = window.WGL || {};

    async function sleep(time) {
        return new Promise((resolve) => {
            window.setTimeout(() => {
                resolve();
            }, time);
        });
    }

    if(window.OffscreenCanvas === undefined)
        throw "OffscreenCanvas not supported. Update your browser or enable the experimental canvas features (I know right, all of that because browsers do things wrong with texImage2D)";
    // firefox: in about:config, set gfx.offscreencanvas.enabled to true

    var worker = new Worker("../src/worker.js?" + new Date().getTime());
    worker.onmessage = function(event) {
        console.log(event.data);
    };

    WGL.Context = class Context {
        /*
         * @param attributes alpha, depth, stencil, antialiasing, premultipliedAlpha, preserveDrawingBuffer, failIfMajorPerformanceCaveat
         */
        constructor(canvas, attributes = {}) {
            var gl;
            this._canvas = canvas;
            var types = ['webgl2', 'experimental-webgl2', 'webgl', 'experimental-webgl'];
            for(var type in types) {
                if(gl = canvas.getContext(types[type], attributes))
                    break;
            }
            if(gl instanceof WebGL2RenderingContext)
                this.webglGersion = 2;
            else if(gl instanceof WebGLRenderingContext)
                this.webglGersion = 1;
            else
                throw "Failed to create webgl context";

            this._gl = gl;
            this._fences = [];
            this._f = undefined;
            this._nextFrameStart = new Date().getTime();
            this._frame = 0;
            this.frame();
        }

        /*
         * Starts a opengl driver async code
         * Code run in f will wait for opengl to complete, data MAY not be ready for current frame
         */
        async(f) {
            return new Promise((resolve) => {
                var gl = this._gl;

                f();

                var fence = gl.fenceSync(gl.SYNC_GPU_COMMANDS_COMPLETE, 0);

                this._fences.push({
                    fence: fence,
                    done: resolve,
                    frame: this._frame,
                    t0: new Date().getTime(),
                });
            });
        }

        /*
         * Checks if opengl driver async tasks are completed.
         * This function can wait until next frame max
         */
        checkFences() {
            var gl = this._gl;

            while(this._nextFrameStart > new Date().getTime()) {
                var taskCompleted = false; // at least one task completed

                // loop trough all fences and wait until frame start at max
                for(var i = this._fences.length - 1; i >= 0; i--) {
                    var fence = this._fences[i];
                    var waitTimeout = Math.max(0, (this._nextFrameStart - new Date().getTime()) / 1000);
                    //if(gl.getSyncParameter(fence.fence, gl.SYNC_STATUS) === gl.SIGNALED) {
                    if(gl.clientWaitSync(fence.fence, 0, waitTimeout)) {
                        // task is completed, remove
                        gl.deleteSync(fence.fence);
                        this._fences = this._fences.slice(0, i).concat(this._fences.slice(i + 1));

                        // signal
                        fence.done();

                        var t = new Date().getTime() - fence.t0;
                        var frames = this._frame - fence.frame;
                        //console.log('task done: ' + t + 'ms, ' + frames + ' frames');

                        taskCompleted = true;
                    }
                }

                if(taskCompleted === false)
                    break;
            }
        }

        update() {
        }

        frame() {
            this._frame++;
            var gl = this._gl;

            this.checkFences();

            // expect 60fps : blocking operations must stop before _nextFrameStart
            this._nextFrameStart = new Date().getTime() + Math.min(1000 / 60, gl.MAX_CLIENT_WAIT_TIMEOUT_WEBGL / 1000);

            this.update();
            this.render();

            window.requestAnimationFrame(() => {
                this.frame();

                // swap the frame and do this between two frames
                window.setTimeout(() => {
                    this.checkFences();
                }, 0);
            });
        }

        render() {
            var gl = this._gl;

            gl.viewport(0, 0, this._canvas.width, this._canvas.height);

            if(this._f)
                this._f();
        }

        main(f) {
            this._f = f;
        }

        clear() {
            var gl = this._gl;
            gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        }

        async createBuffer(attribs, data) {
            var gl = this._gl;

            var buffer = new WGL.Buffer(this);
            await buffer.upload(attribs, data);
            return buffer;
        }

        async createShader(type, source) {
            var gl = this._gl;

            var shader = new WGL.Shader(this, type);
            await shader.compile(source);
            return shader;
        }

        async createProgram(vertex, fragment) {
            var gl = this._gl;

            var program = new WGL.Program(this);
            await program.link(vertex, fragment);
            return program;
        }

        async createTexture(url) {
            var gl = this._gl;

            return new Promise((resolve) => {
                worker.onmessage = (event) => {
                    var texture = new WGL.Texture(this);
                    texture.upload(event.data);
                    resolve(texture);
                };
                worker.postMessage({
                    command: 'createTexture',
                    url: url,
                });
            });
            /*
            await sleep(1000);

            var texture = new WGL.Texture(this);
            texture.upload(image);
            console.log('texture uploaded');

            await sleep(500);

            return texture;*/
        }
    };

    WGL.Buffer = class Buffer {
        constructor(context) {
            this._context = context;
            this._buffers = [];
            this._attributes = [];
            this._count = 0;
        }

        async upload(bufs) {
            var gl = this._context._gl;

            return this._context.async(async () => {
                bufs.forEach((buf) => {
                    var buffer = {
                        buffer: undefined,
                        type: undefined,
                        size: 3,
                        stride: 0,
                        offset: 0,
                        attributes: [],
                        valueSize: 0,
                        normalize: false,
                    };
                    if(Array.isArray(buf.data) || buf.data instanceof Float32Array) {
                        buffer.type = gl.FLOAT;
                        buffer.valueSize = 4;
                    }
                    else if(buf.data instanceof Uint8Array) {
                        buffer.type = gl.UNSIGNED_BYTE;
                        buffer.valueSize = 1;
                        buffer.normalize = true;
                    }
                    //gl.BYTE, gl.UNSIGNED_BYTE, gl.SHORT, gl.UNSIGNED_SHORT, gl.FLOAT
                    else
                        throw "Unsupported format";

                    buffer.buffer = gl.createBuffer();
                    gl.bindBuffer(gl.ARRAY_BUFFER, buffer.buffer);
                    gl.bufferData(gl.ARRAY_BUFFER, Array.isArray(buf.data) ? new Float32Array(buf.data) : buf.data, gl.STATIC_DRAW);
                    this._buffers.push(buffer);

                    var offset = 0;
                    var elementSize = 0;
                    var attributes = [];
                    for(var name in buf.attributes) {
                        var attr = buf.attributes[name];

                        var attribute = {
                            name: name,
                            buffer: buffer.buffer,
                            type: buffer.type,
                            size: 3,
                            stride: 0,
                            offset: offset,
                            normalize: buffer.normalize,
                        };
                        if(attr.size !== undefined)
                            attribute.size = attr.size;
                        elementSize += attribute.size;
                        offset += attribute.size * buffer.valueSize;
                        attributes.push(attribute);
                    }
                    if(this._count === 0)
                        this._count = buf.data.length / elementSize;

                    attributes.forEach((attribute) => {
                        var attr = buf.attributes[attribute.name];
                        attribute.stride = offset;
                        this._attributes.push(attribute);
                    });
                });
            });
        }

        draw(params) {
            var gl = this._context._gl;
            var shader = WGL.Program._current;
            var uniforms = params.uniforms;

            gl.bindBuffer(gl.ARRAY_BUFFER, this._buffer);


            //TODO: use vao https://webgl2fundamentals.org/webgl/lessons/webgl1-to-webgl2.html
            this._attributes.forEach((attribute) => {
                var pos = shader.getAttrib(attribute.name);
                gl.bindBuffer(gl.ARRAY_BUFFER, attribute.buffer);
                gl.vertexAttribPointer(pos, attribute.size, attribute.type, attribute.normalize, attribute.stride, attribute.offset);
                gl.enableVertexAttribArray(pos);
            });

            var texUnit = 0;
            for(var name in uniforms) {
                var uniform = uniforms[name];

                if(uniform instanceof WGL.Texture) {
                    gl.activeTexture(gl.TEXTURE0 + texUnit);
                    gl.bindTexture(gl.TEXTURE_2D, uniform._texture);

                    var loc = shader.getUniform(name);
                    gl.uniform1i(loc, texUnit);

                    texUnit++;
                }
            }

            gl.drawArrays(gl.TRIANGLES, 0, this._count);

            this._attributes.forEach((attribute) => {
                var pos = shader.getAttrib(attribute.name);
                gl.disableVertexAttribArray(pos);
            });
        }
    };

    WGL.Program = class Program {
        constructor(context) {
            this._context = context;
            this._attribs = {};
            this._uniforms = {};
        }

        async link(vertex, fragment) {
            var gl = this._context._gl;

            await this._context.async(async () => {
                function compile(type, source) {
                    var shader = gl.createShader(type);
                    gl.shaderSource(shader, source);
                    gl.compileShader(shader);

                    if(!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
                        throw gl.getShaderInfoLog(shader);
                    }

                    return shader;
                }

                this._program = gl.createProgram();
                gl.attachShader(this._program, compile(gl.VERTEX_SHADER, vertex));
                gl.attachShader(this._program, compile(gl.FRAGMENT_SHADER, fragment));
                gl.linkProgram(this._program);

                if(!gl.getProgramParameter(this._program, gl.LINK_STATUS)) {
                    var info = gl.getProgramInfoLog(program);
                    throw 'Could not compile WebGL program. \n\n' + info;
                }
            });
        }

        getAttrib(name) {
            if(name in this._attribs)
                return this._attribs[name];
            var gl = this._context._gl;
            return this._attribs[name] = gl.getAttribLocation(this._program, name);
        }

        getUniform(name) {
            if(name in this._uniforms)
                return this._uniforms[name];
            var gl = this._context._gl;
            return this._uniforms[name] = gl.getUniformLocation(this._program, name);
        }

        use() {
            this._context._gl.useProgram(this._program);
            WGL.Program._current = this;
        }
    };

    WGL.Texture = class Texture {
        constructor(context) {
            this._context = context;
        }

        async upload(image) {
            var gl = this._context._gl;

            // TODO: put this in a worker
            function imageToUint8Array(image){
                var canvas = document.createElement('canvas'),
                    ctx = canvas.getContext('2d');
                canvas.width = image.width,
                    canvas.height = image.height;
                ctx.drawImage(image, 0, 0);
                var imageData = ctx.getImageData(0, 0, image.width, image.height);
                var buff = new Uint8Array(imageData.data.buffer);
                return buff;
            }

            await this._context.async(async () => {
                this._texture = gl.createTexture();
                gl.bindTexture(gl.TEXTURE_2D, this._texture);
                gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, false);
                gl.pixelStorei(gl.UNPACK_PREMULTIPLY_ALPHA_WEBGL, false);
                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);

                // make an offscreencanvas in a worker to make the conversion, and get an Uint8Array

                //var array = imageToUint8Array(image);
                var array = new Uint8Array(image.buffer);

                var t0 = new Date().getTime();
                gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, image.width, image.height, 0, gl.RGBA, gl.UNSIGNED_BYTE, array);
                console.log('teximage2d: ' + (new Date().getTime() - t0));

                gl.bindTexture(gl.TEXTURE_2D, null);
            });
        }
    };
})();
